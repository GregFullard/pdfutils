/**
 * 
 */
package za.co.innisfree.pdfutils.stamper;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.LineSegment;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

/**
 *
 */
public class MyTextRenderListener implements RenderListener {

	@Override
	public void beginTextBlock() {
		//System.out.print("<");
		
	}

	@Override
	public void endTextBlock() {
		//System.out.print(">");
	}

	@Override
	public void renderImage(ImageRenderInfo renderInfo) {}

	@Override
	public void renderText(TextRenderInfo renderInfo) {
		// Check if this is the text marker
		String text = renderInfo.getText();
		//System.out.println("T:" + text);
		if (text.equalsIgnoreCase("Comments:")) {
			// Found it
			LineSegment ls = renderInfo.getBaseline(); 
			System.out.println("Found at X: " + ls.getBoundingRectange().getX() +
					",  Y: " + ls.getBoundingRectange().getY());
		}
	}
}
