/**
 * 
 */
package za.co.innisfree.pdfutils.notefield;

import static org.junit.Assert.*;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author gregf
 *
 */
public class FileAnalyserTest {

	/**
	 * Test method for {@link za.co.innisfree.pdfutils.stamper.FileAnalyser#analysePdfFile(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void simple1PageFile() throws IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/test.pdf").getFile());
		
		// Now analyse the file
		String inputPath = file.getAbsolutePath();
		FileAnalyser analyser = new FileAnalyser();
		PdfFileProperties props = analyser.analysePdfFile(inputPath);
		
		assertEquals(1, props.getPageCount());
		assertEquals(0.0, props.getLastPageLeftPosition(),0.001);
		assertEquals(0.0, props.getLastPageBottomPosition(),0.001);
		assertEquals(595.2756, props.getLastPageRightPosition(),0.001);
		assertEquals(841.8898, props.getLastPageTopPosition(),0.001);
	
	}
	
	@Test
	public void simple2PageFile() throws IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/test2.pdf").getFile());
		
		// Now analyse the file
		String inputPath = file.getAbsolutePath();
		FileAnalyser analyser = new FileAnalyser();
		PdfFileProperties props = analyser.analysePdfFile(inputPath);
		
		assertEquals(2, props.getPageCount());
		assertEquals(0.0, props.getLastPageLeftPosition(),0.001);
		assertEquals(0.0, props.getLastPageBottomPosition(),0.001);
		assertEquals(595.2756, props.getLastPageRightPosition(),0.001);
		assertEquals(841.8898, props.getLastPageTopPosition(),0.001);
	
	}
	
	@Test
	public void reportSample1() throws IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/SL-20160918-1_04CustVersion.pdf").getFile());
		
		// Now analyse the file
		String inputPath = file.getAbsolutePath();
		FileAnalyser analyser = new FileAnalyser();
		PdfFileProperties props = analyser.analysePdfFile(inputPath);
		
		assertEquals(24, props.getPageCount());
		assertEquals(0.0, props.getLastPageLeftPosition(),0.001);
		assertEquals(0.0, props.getLastPageBottomPosition(),0.001);
		assertEquals(594.0, props.getLastPageRightPosition(),0.001);
		assertEquals(842.0, props.getLastPageTopPosition(),0.001);
	}	

}
