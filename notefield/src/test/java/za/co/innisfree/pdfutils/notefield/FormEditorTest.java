package za.co.innisfree.pdfutils.notefield;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.itextpdf.text.DocumentException;

public class FormEditorTest {

	private boolean checkIfFileExists(String filePath) {
		boolean respMessage = false;
		File f = new File(filePath);

		if (f.exists() && !f.isDirectory()) {
			respMessage = true;
		}
		return respMessage;
	}
	
	/**
	 * Test method for {@link za.co.innisfree.pdfutils.stamper.FormEditor#addButtonToPdf(java.lang.String, java.lang.String)}.
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	@Test
	public void testAddButtonToPdf() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/test.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_Edited");
		FormEditor fe = new FormEditor();
		fe.addButtonToPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}
	
	@Test
	public void testAddFieldToPdf() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/test.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedTextField");
		FormEditor fe = new FormEditor();
		fe.addTextFieldToPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}
	
	@Test
	public void testAddFieldTo2PagePdf() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/test3.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedTextField");
		FormEditor fe = new FormEditor();
		fe.addTextFieldToLastPageOfPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}
	
	@Test
	public void testAddFieldToCalReport() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/SL-20160918-1_04CustVersion.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedTextField");
		FormEditor fe = new FormEditor();
		fe.addTextFieldToLastPageOfPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}	
	
	@Test
	public void testAddFieldToCalReport2() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/SL-20160921-1_01CustVersion.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedTextField");
		FormEditor fe = new FormEditor();
		fe.addTextFieldToLastPageOfPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}	
}
