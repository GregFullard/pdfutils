/**
 * 
 */
package za.co.innisfree.pdfutils.notefield;

/**
 * This class stores the important properties of a file
 * 
 * @author gregf
 *
 */
public class PdfFileProperties {

	int pageCount;
	float lastPageLeftPosition;
	float lastPageBottomPosition;
	float lastPageRightPosition;
	float lastPageTopPosition;
	float textBoxLeftPosition;
	float textBoxBottomPosition;
	float textBoxRightPosition;
	float textBoxTopPosition;
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public float getLastPageLeftPosition() {
		return lastPageLeftPosition;
	}
	public void setLastPageLeftPosition(float lastPageLeftPosition) {
		this.lastPageLeftPosition = lastPageLeftPosition;
	}
	public float getLastPageBottomPosition() {
		return lastPageBottomPosition;
	}
	public void setLastPageBottomPosition(float lastPageBottomPosition) {
		this.lastPageBottomPosition = lastPageBottomPosition;
	}
	public float getLastPageRightPosition() {
		return lastPageRightPosition;
	}
	public void setLastPageRightPosition(float lastPageRightPosition) {
		this.lastPageRightPosition = lastPageRightPosition;
	}
	public float getLastPageTopPosition() {
		return lastPageTopPosition;
	}
	public void setLastPageTopPosition(float lastPageTopPosition) {
		this.lastPageTopPosition = lastPageTopPosition;
	}
	public float getTextBoxLeftPosition() {
		return textBoxLeftPosition;
	}
	public void setTextBoxLeftPosition(float textBoxLeftPosition) {
		this.textBoxLeftPosition = textBoxLeftPosition;
	}
	public float getTextBoxBottomPosition() {
		return textBoxBottomPosition;
	}
	public void setTextBoxBottomPosition(float textBoxBottomPosition) {
		this.textBoxBottomPosition = textBoxBottomPosition;
	}
	public float getTextBoxRightPosition() {
		return textBoxRightPosition;
	}
	public void setTextBoxRightPosition(float textBoxRightPosition) {
		this.textBoxRightPosition = textBoxRightPosition;
	}
	public float getTextBoxTopPosition() {
		return textBoxTopPosition;
	}
	public void setTextBoxTopPosition(float textBoxTopPosition) {
		this.textBoxTopPosition = textBoxTopPosition;
	}
	
	
}
