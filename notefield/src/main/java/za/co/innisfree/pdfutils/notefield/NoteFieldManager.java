/**
 * 
 */
package za.co.innisfree.pdfutils.notefield;

import java.io.IOException;

import com.itextpdf.text.DocumentException;

/**
 * @author gregf
 *
 */
public class NoteFieldManager {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	public static void main(String[] args) throws DocumentException, IOException {
	    String originalFileName = args[0];
	    String updatedFileName = args[1];
		
	    FormEditor formEditor = new FormEditor();
	    formEditor.addTextFieldToLastPageOfPdf(originalFileName, updatedFileName);
	}
}
