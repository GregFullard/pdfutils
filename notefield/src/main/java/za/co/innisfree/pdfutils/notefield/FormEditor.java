/**
 * 
 */
package za.co.innisfree.pdfutils.notefield;


import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.TextField;
 
public class FormEditor {
 
    /** The resulting PDF file. */
    public static final String RESULT
        = "results/part3/chapter09/submit_me.pdf";
    
    public void addButtonToPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PushbuttonField button = new PushbuttonField(
            stamper.getWriter(), new Rectangle(36, 700, 72, 730), "post");
        button.setText("POST");
        button.setBackgroundColor(new GrayColor(0.7f));
        button.setVisibility(PushbuttonField.VISIBLE_BUT_DOES_NOT_PRINT);
        PdfFormField submit = button.getField();
        submit.setAction(PdfAction.createSubmitForm(
            "http://itextpdf.com:8180/book/request", null,
            PdfAction.SUBMIT_HTML_FORMAT | PdfAction.SUBMIT_COORDINATES));
        stamper.addAnnotation(submit, 1);
        stamper.close();
    }
    
    public void addTextFieldToPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        TextField textField = new TextField(
            stamper.getWriter(), new Rectangle(36, 700, 200, 730), "Comments");
        textField.setBackgroundColor(new BaseColor(255,255,255)); 
        textField.setBorderColor(new BaseColor(0,0,0)); 
        textField.setBorderWidth(1); 
        textField.setBorderStyle(PdfBorderDictionary.STYLE_SOLID); 
        textField.setText(""); 
        textField.setAlignment(Element.ALIGN_LEFT); 
        textField.setOptions(TextField.REQUIRED);  
        textField.setVisibility(TextField.VISIBLE);
        PdfFormField tf1 = textField.getTextField();
        stamper.addAnnotation(tf1, 1);
        stamper.close();
    }
    
    public void addTextFieldToLastPageOfPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        System.out.println("Number of pages is: " + reader.getNumberOfPages());
        int lastPage = reader.getNumberOfPages();
        Rectangle mediabox = reader.getPageSize(lastPage);
        
        float left = mediabox.getLeft();
        float right = mediabox.getRight();
        float bottom = mediabox.getBottom();
        float top = mediabox.getTop();
        
        System.out.println("Mediabox on last page");
        System.out.print(left);
        System.out.print(',');
        System.out.print(bottom);
        System.out.print(',');
        System.out.print(right);
        System.out.print(',');
        System.out.print(top);
        System.out.println("]");
        
        //float leftField = left + 100;
        //float rightField = right - 100;
        //float bottomField = bottom + 100;
        //float topField = bottomField - 30;
        
        float leftField = 27;
        float rightField = right - 27;
        float bottomField = 180 ;
        float topField = bottomField - 150;
        
        TextField textField = new TextField(
            stamper.getWriter(), new Rectangle(leftField, topField, rightField, bottomField), "Veterinarian Comments:");
        textField.setBackgroundColor(new BaseColor(255,255,255)); 
        textField.setBorderColor(new BaseColor(0,0,0)); 
        textField.setBorderWidth(1); 
        textField.setBorderStyle(PdfBorderDictionary.STYLE_SOLID); 
        textField.setText(""); 
        textField.setAlignment(Element.ALIGN_LEFT); 
        textField.setOptions(TextField.REQUIRED);  
        textField.setOptions(TextField.MULTILINE);
        textField.setVisibility(TextField.VISIBLE);
        textField.setFontSize(8);
        PdfFormField tf1 = textField.getTextField();
        stamper.addAnnotation(tf1, lastPage);
        stamper.close();
    }    
}
