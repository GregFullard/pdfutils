/**
 * 
 */
package za.co.innisfree.pdfutils.stamper;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.io.RandomAccessSource;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import com.itextpdf.text.pdf.parser.RenderListener;

import za.co.innisfree.pdfutils.stamper.model.PdfFileProperties;

/**
 * Utilities for analysing a supplied file
 * 
 * @author gregf
 *
 */
public class FileAnalyser {
	
	final static Logger logger = Logger.getLogger(FileAnalyser.class);

	
	
	
	public PdfFileProperties analysePdfFile(String src) throws IOException {
		PdfFileProperties props = new PdfFileProperties();
        PdfReader reader = new PdfReader(src);
        
        // Get Page Count
        int lastPage = reader.getNumberOfPages();
        props.setPageCount(lastPage);
        logger.debug("Number of pages is: " + lastPage);
        
        // Get the page size of last page
        Rectangle mediabox = reader.getPageSize(lastPage);
        float left = mediabox.getLeft();
        float right = mediabox.getRight();
        float bottom = mediabox.getBottom();
        float top = mediabox.getTop();	
        props.setLastPageLeftPosition(left);
        props.setLastPageRightPosition(right);
        props.setLastPageBottomPosition(bottom);
        props.setLastPageTopPosition(top);
        logger.debug("Coordinates of last page [L, B, R, T] is: [" +
        		left + ", " + bottom + ", " + right + ", " + top + "]");
		
        // Do Old-school parsing
		//logger.debug("=============================");
		//logger.debug("Old school parsing");
        //parsePdfPage(src,lastPage);
        
        // Improved parsing
		logger.debug("=============================");
		logger.debug("Parse with listener");
        parsePdfPage2(src,lastPage);        
        
        
		return props;
	}
	
	public PdfFileProperties analysePolicyCentreAfr(String src) throws IOException {
		PdfFileProperties props = new PdfFileProperties();
        PdfReader reader = new PdfReader(src);
        
        // Get Page Count
        int lastPage = reader.getNumberOfPages();
        props.setPageCount(lastPage);
        logger.debug("Number of pages is: " + lastPage);
        
        // Get the page size of first page
        Rectangle firstMediabox = reader.getPageSize(1);
        float firstLeft = firstMediabox.getLeft();
        float firstRight = firstMediabox.getRight();
        float firstBottom = firstMediabox.getBottom();
        float firstTop = firstMediabox.getTop();	
        props.setLastPageLeftPosition(firstLeft);
        props.setLastPageRightPosition(firstRight);
        props.setLastPageBottomPosition(firstBottom);
        props.setLastPageTopPosition(firstTop);
        logger.debug("Coordinates of first page [L, B, R, T] is: [" +
        		firstLeft + ", " + firstBottom + ", " + firstRight + ", " + firstTop + "]");        
        
        // Get the page size of last page
        Rectangle lastMediabox = reader.getPageSize(lastPage);
        float lastLeft = lastMediabox.getLeft();
        float lastRight = lastMediabox.getRight();
        float lastBottom = lastMediabox.getBottom();
        float lastTop = lastMediabox.getTop();	
        props.setLastPageLeftPosition(lastLeft);
        props.setLastPageRightPosition(lastRight);
        props.setLastPageBottomPosition(lastBottom);
        props.setLastPageTopPosition(lastTop);
        logger.debug("Coordinates of last page [L, B, R, T] is: [" +
        		lastLeft + ", " + lastBottom + ", " + lastRight + ", " + lastTop + "]");
		
        // Do Old-school parsing
		//logger.debug("=============================");
		//logger.debug("Old school parsing");
        //parsePdfPage(src,lastPage);
        
        // Improved parsing
		logger.debug("=============================");
		logger.debug("Parse with listener");
        parsePdfPage2(src,lastPage);        
        
        
		return props;
	}	
	
	/*
	 * Old-school method
	 */
	private void parsePdfPage(String src, int pageNum) throws IOException {
		PdfReader reader = new PdfReader(src);
		byte[] streamBytes = reader.getPageContent(pageNum);
		RandomAccessSourceFactory rasFactory = new RandomAccessSourceFactory();
		RandomAccessSource ras = rasFactory.createSource(streamBytes); 
		RandomAccessFileOrArray rafa = new RandomAccessFileOrArray(ras);
		PRTokeniser tokeniser = new PRTokeniser(rafa);
		while (tokeniser.nextToken()) {
			if (tokeniser.getTokenType()==PRTokeniser.TokenType.STRING) {
				System.out.println(tokeniser.getStringValue());
			}
		}
	}
	
	private void parsePdfPage2(String src, int pageNum) throws IOException {
		PdfReader reader = new PdfReader(src);
		
		RenderListener listener = new MyTextRenderListener();
		PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
		PdfDictionary pageDic = reader.getPageN(pageNum);
		PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
		processor.processContent(ContentByteUtils.getContentBytesForPage(reader, pageNum), resourcesDic);
	}
	
}
