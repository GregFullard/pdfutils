/**
 * 
 */
package za.co.innisfree.pdfutils.stamper;

import java.awt.Color;
 
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.TextField;
 
public class PdfDateEditor {
 
    /** The resulting PDF file. */
    public static final String RESULT
        = "results/part3/chapter09/submit_me.pdf";
    
    public void addButtonToPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PushbuttonField button = new PushbuttonField(
            stamper.getWriter(), new Rectangle(36, 700, 72, 730), "post");
        button.setText("POST");
        button.setBackgroundColor(new GrayColor(0.7f));
        button.setVisibility(PushbuttonField.VISIBLE_BUT_DOES_NOT_PRINT);
        PdfFormField submit = button.getField();
        submit.setAction(PdfAction.createSubmitForm(
            "http://itextpdf.com:8180/book/request", null,
            PdfAction.SUBMIT_HTML_FORMAT | PdfAction.SUBMIT_COORDINATES));
        stamper.addAnnotation(submit, 1);
        stamper.close();
    }
    
    public void addTextFieldToPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        TextField textField = new TextField(
            stamper.getWriter(), new Rectangle(36, 700, 200, 730), "Comments");
        textField.setBackgroundColor(new BaseColor(255,255,255)); 
        textField.setBorderColor(new BaseColor(0,0,0)); 
        textField.setBorderWidth(1); 
        textField.setBorderStyle(PdfBorderDictionary.STYLE_SOLID); 
        textField.setText(""); 
        textField.setAlignment(Element.ALIGN_LEFT); 
        textField.setOptions(TextField.REQUIRED);  
        textField.setVisibility(TextField.VISIBLE);
        PdfFormField tf1 = textField.getTextField();
        stamper.addAnnotation(tf1, 1);
        stamper.close();
    }
    
    public void addTextFieldToLastPageOfPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        System.out.println("Number of pages is: " + reader.getNumberOfPages());
        int lastPage = reader.getNumberOfPages();
        Rectangle mediabox = reader.getPageSize(lastPage);
        
        float left = mediabox.getLeft();
        float right = mediabox.getRight();
        float bottom = mediabox.getBottom();
        float top = mediabox.getTop();
        
        System.out.println("Mediabox on last page");
        System.out.print(left);
        System.out.print(',');
        System.out.print(bottom);
        System.out.print(',');
        System.out.print(right);
        System.out.print(',');
        System.out.print(top);
        System.out.println("]");
        
        //float leftField = left + 100;
        //float rightField = right - 100;
        //float bottomField = bottom + 100;
        //float topField = bottomField - 30;
        
        float leftField = 27;
        float rightField = right - 27;
        float bottomField = 165 - 10;
        float topField = bottomField - 100;
        
        TextField textField = new TextField(
            stamper.getWriter(), new Rectangle(leftField, topField, rightField, bottomField), "Comments");
        textField.setBackgroundColor(new BaseColor(255,255,255)); 
        textField.setBorderColor(new BaseColor(0,0,0)); 
        textField.setBorderWidth(1); 
        textField.setBorderStyle(PdfBorderDictionary.STYLE_SOLID); 
        textField.setText(""); 
        textField.setAlignment(Element.ALIGN_LEFT); 
        textField.setOptions(TextField.REQUIRED);  
        textField.setOptions(TextField.MULTILINE);
        textField.setVisibility(TextField.VISIBLE);
        textField.setFontSize(8);
        PdfFormField tf1 = textField.getTextField();
        stamper.addAnnotation(tf1, lastPage);
        stamper.close();
    }    
    
    public void addDateToAllPagesOfPdf(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        System.out.println("Number of pages is: " + reader.getNumberOfPages());
        int lastPage = reader.getNumberOfPages();
        
        BaseFont bf = BaseFont.createFont(
                BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED); // set font

        float left;
        float right;
        float bottom;
        float top;
        
        //loop on pages (1-based)
        for (int i=1; i<=lastPage; i++){
        	
            Rectangle mediabox = reader.getPageSize(i);
            
            left = mediabox.getLeft();
            right = mediabox.getRight();
            bottom = mediabox.getBottom();
            top = mediabox.getTop();
            
            System.out.print("Mediabox on page (" + i + "): ");
            System.out.print(left);
            System.out.print(',');
            System.out.print(bottom);
            System.out.print(',');
            System.out.print(right);
            System.out.print(',');
            System.out.print(top);
            System.out.println("]");            

            // get object for writing over the existing content;
            // you can also use getUnderContent for writing in the bottom layer
            PdfContentByte over = stamper.getOverContent(i);

            // Add a white rectangle over the existing date
            Rectangle rect = new Rectangle(440, 580, 520, 590);
            rect.setBorder(Rectangle.BOX);
            rect.setBackgroundColor(BaseColor.WHITE);
            rect.setBorderWidth(0);
            over.rectangle(rect);
            
            // write new date
            over.beginText();
            over.setColorFill(BaseColor.BLACK);
            over.setFontAndSize(bf, 8);    // set font and size
            over.setTextMatrix(445, 583);   // set x,y position (0,0 is at the bottom left)
            over.showText("DD Month CCCC");  // set text
            over.endText();

        }        

        stamper.close();
    } 
    
    public void changeSolimarAfrDate(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        System.out.println("Number of pages is: " + reader.getNumberOfPages());
        int lastPage = reader.getNumberOfPages();
        
        BaseFont bf = BaseFont.createFont(
                BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED); // set font

        float left;
        float right;
        float bottom;
        float top;
        
        // change Date on Page 1 Only
        int i = 1;
        Rectangle mediabox = reader.getPageSize(i);
        
        left = mediabox.getLeft();
        right = mediabox.getRight();
        bottom = mediabox.getBottom();
        top = mediabox.getTop();
        
        System.out.print("Mediabox on page (" + i + "): ");
        System.out.print(left);
        System.out.print(',');
        System.out.print(bottom);
        System.out.print(',');
        System.out.print(right);
        System.out.print(',');
        System.out.print(top);
        System.out.println("]");            

        // get object for writing over the existing content;
        // you can also use getUnderContent for writing in the bottom layer
        PdfContentByte over = stamper.getOverContent(i);

        // Add a white rectangle over the existing date
        Rectangle rect = new Rectangle(355, 685, 500, 700);
        rect.setBorder(Rectangle.BOX);
        rect.setBackgroundColor(BaseColor.WHITE);
        rect.setBorderWidth(0);
        over.rectangle(rect);
        
        // write new date
        over.beginText();
        over.setColorFill(BaseColor.BLACK);
        over.setFontAndSize(bf, 9);    // set font and size
        over.setTextMatrix(357, 689);   // set x,y position (0,0 is at the bottom left)
        over.showText("DD Month CCCC");  // set text
        over.endText();             

        stamper.close();
    }     
    
    public void changeTransformAfrDate(String src, String dest) throws DocumentException, IOException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        
        System.out.println("Number of pages is: " + reader.getNumberOfPages());
        int lastPage = reader.getNumberOfPages();
        
        BaseFont bf = BaseFont.createFont(
                BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED); // set font

        float left;
        float right;
        float bottom;
        float top;
        
        // change Date on Page 1 Only
        int i = 1;
        Rectangle mediabox = reader.getPageSize(i);
        
        left = mediabox.getLeft();
        right = mediabox.getRight();
        bottom = mediabox.getBottom();
        top = mediabox.getTop();
        
        System.out.print("Mediabox on page (" + i + "): ");
        System.out.print(left);
        System.out.print(',');
        System.out.print(bottom);
        System.out.print(',');
        System.out.print(right);
        System.out.print(',');
        System.out.print(top);
        System.out.println("]");            

        // get object for writing over the existing content;
        // you can also use getUnderContent for writing in the bottom layer
        PdfContentByte over = stamper.getOverContent(i);

        // Add a white rectangle over the existing date
        Rectangle rect = new Rectangle(355, 685, 500, 700);
        rect.setBorder(Rectangle.BOX);
        rect.setBackgroundColor(BaseColor.WHITE);
        rect.setBorderWidth(0);
        over.rectangle(rect);
        
        // write new date
        over.beginText();
        over.setColorFill(BaseColor.BLACK);
        over.setFontAndSize(bf, 9);    // set font and size
        over.setTextMatrix(357, 689);   // set x,y position (0,0 is at the bottom left)
        over.showText("DD Month CCCC");  // set text
        over.endText();             

        stamper.close();
    }        
}
