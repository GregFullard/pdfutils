/**
 * 
 */
package za.co.innisfree.pdf.stamper;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.itextpdf.text.DocumentException;

import za.co.innisfree.pdfutils.stamper.FormEditor;
import za.co.innisfree.pdfutils.stamper.PdfDateEditor;

/**
 * @author gregf
 *
 */
public class PdfDateEditorTest {

	private boolean checkIfFileExists(String filePath) {
		boolean respMessage = false;
		File f = new File(filePath);

		if (f.exists() && !f.isDirectory()) {
			respMessage = true;
		}
		return respMessage;
	}
	

	@Test
	public void testAddDateToPolicyCentreAfr() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/Policycenter_PL_Afr_displayDocument.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedDate");
		PdfDateEditor pde = new PdfDateEditor();
		pde.addDateToAllPagesOfPdf(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}	
	
	
	@Test
	public void testAddDateToSolimarAfr() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/Solimar_Afr_getdocument.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedDate");
		PdfDateEditor pde = new PdfDateEditor();
		pde.changeSolimarAfrDate(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}	
	
	@Test
	public void testAddDateToTransformAfr() throws DocumentException, IOException {
		// Read the test file
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("test-files/Transform_PL_Afr_getdocument.pdf").getFile());
		
		// Now add the new content to the PDF
		String inputPath = file.getAbsolutePath();
		String outputPath = inputPath.concat("_AddedDate");
		PdfDateEditor pde = new PdfDateEditor();
		pde.changeTransformAfrDate(inputPath, outputPath);
		
		assertTrue(checkIfFileExists(outputPath));	
	}		
	
	
}
